/*
    Purpose: 
    Author: Reece W. 
    License: All Rights Reserved J. Reece Wilson
*/  
#pragma once
#include <kernel/peloader/pe_loader.h>

error_t pe_loader_init();