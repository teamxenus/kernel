/*
    Purpose:
    Author: Reece W.
    License: All Rights Reserved J. Reece Wilson
*/

#pragma once
size_t stack_realigner(size_t(*)(size_t), size_t);