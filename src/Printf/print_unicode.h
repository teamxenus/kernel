/*
    Purpose: Stage 3 floating point and unicode support 
    Author: Reece W.
	License: All rights reserved (2018) J. Reece Wislon 

*/  
void xenus_printf_write_unicode(void * glhf, void *data, putcf putf, printf_state_ref current, printf_write_all_t write);