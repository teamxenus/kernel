/*
    Purpose:
    Author: Reece W.
    License: All Rights Reserved J. Reece Wilson
*/
#include <xenus.h>
#include "../Boot/access_system.h"
#include <kernel/libx/xenus_libx.h>
#include "shutdown_main.h"
#include "../StackFixup/stack_realigner.h"

static linked_list_head_p shutdown_handlers;

// trying to debug a random system hang...
// can't figure this shit out
// no panic
// no oops 
// just VM freeze
// i can't even be arsed use the canerous kdbg without writing a viusal studio IO kdbg driver
// fuck shutting down for now

//size_t _on_shutdown(size_t a)
//{
//    printf("shutting down!\n");
//    for (linked_list_entry_p current = shutdown_handlers->bottom; current != NULL; current = current->next)
//        (*(shutdown_handler_p*)current->data)();
//    printf("shutting down!\n");
//    return 0;
//}
//
//#define XENUS_THREAD_TLS_START	thread_storage_data_p tls_;  tls_ = tls(); preempt_disable();
//#define XENUS_THREAD_TLS_END	preempt_enable();
//
//XENUS_EXPORT void a()
//{
//
//    printf("aa\n");
//}
//
//void __try_install_switch_hooks(thread_storage_data_p tls_);
//XENUS_EXPORT bool thread_fpu_lock_2()
//{
//    preempt_disable();
//    thread_storage_data_p tls_;  tls_ = tls();
//
//	if (tls_->fpu_enabled_via_hook)
//	{
//        preempt_enable();
//		return false;
//	}
//
//	tls_->fpu_enabled_via_hook = true;
//	__kernel_fpu_begin();
//
//	__try_install_switch_hooks(tls_);
//
//    preempt_enable();
//	return true;
//}
//
void on_shutdown()
{
    //bool mng_fpu;
    //thread_storage_data_p tls_ = tls();
    //
    //mng_fpu = !tls_->fpu_enabled_via_hook;
    //
    //thread_fpu_lock_2();
   // if (mng_fpu)
   //     thread_fpu_lock();
   // //
   // //stack_realigner(_on_shutdown, 0);
   // //
   // if (mng_fpu)
   //     thread_fpu_unlock();
}

void shutdown_add_hook(shutdown_handler_p handler)
{
    linked_list_entry_p entry;
    
    entry = linked_list_append(shutdown_handlers, sizeof(shutdown_handlers));
    
    if (!entry)
        panic("couldn't append shutdown handler");

    *(shutdown_handler_p*)entry->data = handler;
}

void shutdown_init(void)
{
    shutdown_handlers = linked_list_create();
    
    if (!shutdown_handlers)
        panic("out of memory.");

    //reload(on_shutdown);
}